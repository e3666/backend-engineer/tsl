# Sumário

- [**TSL**](#tsl)
    - [TSL 1.2](#tsl-12)
    - [Diffie Hellman](#diffie-hellman)
    - [TLS 1.3](#tls-13)
- [**Referência**](#referência)


# TSL

Para iniciar o estudo em TSL veja o conteúdo de [HTTP](https://gitlab.com/e3666/backend-engineer/communication-protocols#http), mais especificamente a parte em que falo do handshake. E veja que a comunicação no HTTPS só é segura pois existe um criptografia com uma chave que client e server possuem. E falando em criptografia, veja também o material de [criptografia](https://gitlab.com/e3666/backend-engineer/encryption)

## TSL 1.2

Vamos abrir um pouco mais o processo de handshake, para explicar como funciona o TSL (Transport Layer Security). 

- Primeiro a conexão TCP é aberta
- O client envia um "olá" para o servidor, informando quais os tipos de protocolos de criptografia ele aceita (as vezes diferentes browsers suportam diferentes protocolos - isso antigamente)
- Com base no protocolo que o client pede, o server envia o certificado (chave pública)
- Em posse da chave pública do servidor, o client encripta uma chave simétrica e a envia para o server
- Agora o server decodifica a chave simétrica, e avisa o client que ela chegou. E agora o client pode fazer as requests e o server as respostas com base nessa chave simétrica.

Mas aqui tem um problema, o envio da chave simétrica. Por mais que ela esteja criptografada com a chave pública do servidor, ainda existe um risco de alguém conseguir acesso ao server e pegar a chave privada (por mais estranho que isso pareça, não é impossível).

Outro problema, é a velocidade disso. Pois entre a conexão TCP ser aberta e o envio da primeira request foram 4 passos.

## Diffie Hellman

Diferente do algoritmo de RSA, aqui vamos ter 3 chaves, duas privadas, e uma pública. Porém, a ideia aqui é que a composição dessas três chaves, irá gerar a chave privada simétrica. Sendo que uma chave privada mais a publica podem ser enviadas, pois são inquebráveis.

Ou seja:

Pública = Pública
Privada = Privada
Pública + Privada = Pública
Pública + Privada + Privada = Chave simétrica

Veja mais sobre esse algoritmo e com detalhes no vídeo da referencia: [Secret Key Exchange (Diffie-Hellman) - Computerphile](https://www.youtube.com/watch?v=NmM9HA2MQGI)


## TLS 1.3

Agora o client não tem mais opção de escolher qual algoritmo de criptografia utilizar, sempre será Diffie Helman. O Processo agora fica da seguinte forma:

- O client gera uma chave pública, e uma privada
- O client envia a chave pública, e a combinação da sua chave pública e privada
- O servidor recebe a chave pública do client, e a combinação pública+privada do client, e gera a chave privada dele. Ou seja, agora o server possui as três chaves. E com isso ele gera a chave simétrica.
- O servidor envia a chave pública do client combinada com a chave privada dele
- Agora o client possui a combinação das três chaves, e gera a chave simétrica igual a do servidor. E pronto. Agora os dois lados possuem a chave simétrica.


E todo esse processo de handshake acontece em duas etapas de comunicação (enviar as chaves para o server e receber as chaves do server).

- Explicando de uma outra forma:

Existem 3 traves, A, B, C e D, sendo A pública B e C privadas e D simétrica.

O cliente é dono de A e B.
O server é dono de C

- O client envia A e A+B
- O server agora possui A, A+B e C
- O server faz A+B+C e gera D
- O server envia A+C
- O client agora possui A, B e A+C
- O client faz A+C+B e gera D
- Agora ambos os lados possuem D

# Referência

- [Transport Layer Security, TLS 1.2 and 1.3 (Explained by Example) - Hussein Nasser](https://www.youtube.com/watch?v=AlE5X1NlHgg&list=PLQnljOFTspQUNnO4p00ua_C5mKTfldiYT&index=9)
- [Secret Key Exchange (Diffie-Hellman) - Computerphile](https://www.youtube.com/watch?v=NmM9HA2MQGI)
- [Diffie Hellman -the Mathematics bit - Computerphile](https://www.youtube.com/watch?v=Yjrfm_oRO0w)